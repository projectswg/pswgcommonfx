package com.projectswg.common.javafx.intent;

import com.projectswg.common.javafx.hotswap.HotswapComponent;
import me.joshlarson.jlcommon.control.Intent;

public class HotswapNodeRegistrationIntent extends Intent {
	
	private final String componentName;
	private final HotswapComponent component;
	
	public HotswapNodeRegistrationIntent(String componentName, HotswapComponent component) {
		this.componentName = componentName;
		this.component = component;
	}
	
	public String getComponentName() {
		return componentName;
	}
	
	public HotswapComponent getComponent() {
		return component;
	}
	
	public static void broadcast(String componentName, HotswapComponent component) {
		new HotswapNodeRegistrationIntent(componentName, component).broadcast();
	}
	
}
