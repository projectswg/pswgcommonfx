package com.projectswg.common.javafx.intent;

import javafx.scene.Node;
import me.joshlarson.jlcommon.control.Intent;

public class HotswapIntent extends Intent {
	
	private final String component;
	private final Node content;
	private final boolean addToStack;
	
	public HotswapIntent(String component, Node content, boolean addToStack) {
		this.component = component;
		this.content = content;
		this.addToStack = addToStack;
	}
	
	public String getComponent() {
		return component;
	}
	
	public Node getContent() {
		return content;
	}
	
	public boolean isAddToStack() {
		return addToStack;
	}
	
	public static void broadcast(String component, Node content, boolean addToStack) {
		new HotswapIntent(component, content, addToStack).broadcast();
	}
	
}
