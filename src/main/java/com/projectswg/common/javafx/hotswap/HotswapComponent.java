package com.projectswg.common.javafx.hotswap;

import javafx.scene.Node;
import javafx.scene.layout.VBox;

public class HotswapComponent extends VBox {
	
	public HotswapComponent() {
		
	}
	
	public void setContent(Node node) {
		getChildren().setAll(node);
	}
	
}
