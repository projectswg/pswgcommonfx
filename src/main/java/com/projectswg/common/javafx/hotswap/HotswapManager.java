package com.projectswg.common.javafx.hotswap;

import me.joshlarson.jlcommon.control.IntentManager;
import me.joshlarson.jlcommon.log.Log;
import com.projectswg.common.javafx.intent.HotswapIntent;
import com.projectswg.common.javafx.intent.HotswapNodeRegistrationIntent;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class HotswapManager {
	
	private static final AtomicReference<HotswapManager> INSTANCE = new AtomicReference<>(null);
	
	private final AtomicReference<Consumer<HotswapIntent>> hotswapConsumer;
	private final AtomicReference<Consumer<HotswapNodeRegistrationIntent>> registrationConsumer;
	private final Map<String, HotswapComponent> components;
	
	private HotswapManager() {
		this.hotswapConsumer = new AtomicReference<>(null);
		this.registrationConsumer = new AtomicReference<>(null);
		this.components = new ConcurrentHashMap<>();
	}
	
	private void start() {
		IntentManager.getInstance().registerForIntent(HotswapNodeRegistrationIntent.class, registrationConsumer.getAndSet(this::handleHotswapNodeRegistrationIntent));
		IntentManager.getInstance().registerForIntent(HotswapIntent.class, hotswapConsumer.getAndSet(this::handleHotswapRequest));
	}
	
	private void stop() {
		IntentManager.getInstance().unregisterForIntent(HotswapIntent.class, hotswapConsumer.getAndSet(null));
		IntentManager.getInstance().unregisterForIntent(HotswapNodeRegistrationIntent.class, registrationConsumer.getAndSet(null));
	}
	
	private void handleHotswapRequest(HotswapIntent hi) {
		HotswapComponent component = components.get(hi.getComponent());
		if (component == null) {
			Log.w("Unknown HotswapComponent for name: '%s'", hi.getComponent());
			return;
		}
		component.setContent(hi.getContent());
	}
	
	private void handleHotswapNodeRegistrationIntent(HotswapNodeRegistrationIntent hnri) {
		HotswapComponent replaced = components.put(hnri.getComponentName(), hnri.getComponent());
		if (replaced != null)
			Log.w("Replaced HotswapComponent for name '%s'", hnri.getComponentName());
	}
	
	public static void initialize() {
		HotswapManager manager = new HotswapManager();
		HotswapManager old = INSTANCE.getAndSet(manager);
		manager.start();
		if (old != null)
			old.stop();
	}
	
	public static void terminate() {
		HotswapManager manager = INSTANCE.get();
		if (manager != null)
			manager.stop();
	}
	
	public static HotswapManager getInstance() {
		HotswapManager manager = INSTANCE.get();
		if (manager == null)
			throw new IllegalStateException("Can't get instance for HotswapManager");
		return manager;
	}
	
}
