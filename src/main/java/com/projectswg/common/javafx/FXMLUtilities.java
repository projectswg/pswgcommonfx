/***********************************************************************************
* Copyright (c) 2015 /// Project SWG /// www.projectswg.com                        *
*                                                                                  *
* ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on           *
* July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies.  *
* Our goal is to create an emulator which will provide a server for players to     *
* continue playing a game similar to the one they used to play. We are basing      *
* it on the final publish of the game prior to end-game events.                    *
*                                                                                  *
* This file is part of Holocore.                                                   *
*                                                                                  *
* -------------------------------------------------------------------------------- *
*                                                                                  *
* Holocore is free software: you can redistribute it and/or modify                 *
* it under the terms of the GNU Affero General Public License as                   *
* published by the Free Software Foundation, either version 3 of the               *
* License, or (at your option) any later version.                                  *
*                                                                                  *
* Holocore is distributed in the hope that it will be useful,                      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
* GNU Affero General Public License for more details.                              *
*                                                                                  *
* You should have received a copy of the GNU Affero General Public License         *
* along with Holocore.  If not, see <http://www.gnu.org/licenses/>.                *
*                                                                                  *
***********************************************************************************/
package com.projectswg.common.javafx;

import me.joshlarson.jlcommon.log.Log;
import com.projectswg.common.utilities.LocalUtilities;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class FXMLUtilities {
	
	private static final Set<FXMLController> CONTROLLERS = new CopyOnWriteArraySet<>();
	
	public static void terminate() {
		Log.i("Terminating FXML controllers");
		for (FXMLController controller : CONTROLLERS) {
			controller.terminate();
		}
		CONTROLLERS.clear();
	}
	
	public static ResourceBundle getResourceBundle(Locale locale) {
		return ResourceBundle.getBundle("bundles.strings.strings", locale, ResourceUtilities.getPrimarySource().getClassLoader());
	}
	
	public static void onFxmlLoaded(FXMLController controller) {
		CONTROLLERS.add(controller);
	}
	
	public static void showErrorMessage(String title, String message) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.setContentText(message);
		
		alert.showAndWait();
	}
	
	/**
	 * Loads the FXML via File relative to the source directory
	 * @param fxml the path to the fxml file
	 * @param locale the locale for the FXML ResourceBundle
	 * @return the FXMLController after it's been loaded
	 */
	public static FXMLController loadFxml(String fxml, Locale locale) {
		File file = new File(ResourceUtilities.getSourceDirectory(), fxml);
		if (!file.isFile()) {
			Log.e("Unable to load fxml - doesn't exist: %s", file);
			return null;
		}
		return loadFXML(file, locale);
	}

	/**
	 * Loads the FXML via File relative to the local installation directory
	 * @param fxml the path to the fxml file
	 * @param locale the locale for the FXML ResourceBundle
	 * @return the FXMLController after it's been loaded
	 */
	public static FXMLController loadLocalFxml(String fxml, Locale locale) {
		File file = new File(LocalUtilities.getApplicationDirectory(), fxml);
		if (!file.isFile()) {
			Log.e("Unable to load fxml - doesn't exist: %s", file);
			return null;
		}
		return loadFXML(file, locale);
	}

	private static FXMLController loadFXML(File file, Locale locale) {
		try {
			Log.i("Loading fxml: %s", file);
			FXMLLoader fxmlLoader = new FXMLLoader(file.toURI().toURL());
			fxmlLoader.setResources(getResourceBundle(locale));
			fxmlLoader.load();
			onFxmlLoaded(fxmlLoader.getController());
			return fxmlLoader.getController();
		} catch (IOException e) {
			Log.e("Error loading fmxl: %s", file);
			Log.e(e);
			return null;
		}
	}
	
	/**
	 * Loads the FXML through the Class getResourceAsStream implementation
	 * @param fxml the path to the fxml file
	 * @param locale the locale for the FXML ResourceBundle
	 * @return the FXMLController after it's been loaded
	 */
	public static FXMLController loadFxmlAsClassResource(String fxml, Locale locale) {
		try {
			Log.i("Loading fxml: %s", fxml);
			InputStream is = ResourceUtilities.getSourceResourceAsStream(fxml);
			if (is == null) {
				Log.e("Unable to load fxml - doesn't exist: %s", fxml);
				return null;
			}
			FXMLLoader fxmlLoader = new FXMLLoader(ResourceUtilities.getSourceResource(fxml));
			fxmlLoader.setResources(getResourceBundle(locale));
			fxmlLoader.load(is);
			onFxmlLoaded(fxmlLoader.getController());
			return fxmlLoader.getController();
		} catch (IOException | MissingResourceException e) {
			Log.e("Error loading fmxl: %s", fxml);
			Log.e(e);
			return null;
		}
	}
	
}
