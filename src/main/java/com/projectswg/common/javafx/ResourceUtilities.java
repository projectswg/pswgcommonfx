/***********************************************************************************
* Copyright (c) 2015 /// Project SWG /// www.projectswg.com                        *
*                                                                                  *
* ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on           *
* July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies.  *
* Our goal is to create an emulator which will provide a server for players to     *
* continue playing a game similar to the one they used to play. We are basing      *
* it on the final publish of the game prior to end-game events.                    *
*                                                                                  *
* This file is part of Holocore.                                                   *
*                                                                                  *
* -------------------------------------------------------------------------------- *
*                                                                                  *
* Holocore is free software: you can redistribute it and/or modify                 *
* it under the terms of the GNU Affero General Public License as                   *
* published by the Free Software Foundation, either version 3 of the               *
* License, or (at your option) any later version.                                  *
*                                                                                  *
* Holocore is distributed in the hope that it will be useful,                      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
* GNU Affero General Public License for more details.                              *
*                                                                                  *
* You should have received a copy of the GNU Affero General Public License         *
* along with Holocore.  If not, see <http://www.gnu.org/licenses/>.                *
*                                                                                  *
***********************************************************************************/
package com.projectswg.common.javafx;

import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicReference;

public class ResourceUtilities {
	
	private static final AtomicReference<Class<?>> SOURCE = new AtomicReference<>(null);
	private static final AtomicReference<File> SOURCE_DIRECTORY = new AtomicReference<>(null);
	
	static {
		setPrimarySource(ResourceUtilities.class);
	}
	
	public static void setPrimarySource(Class<?> c) {
		SOURCE.set(c);
		try {
			SOURCE_DIRECTORY.set(new File(c.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile());
		} catch (URISyntaxException e) {
			SOURCE_DIRECTORY.set(null);
		}
	}
	
	public static Class<?> getPrimarySource() {
		return SOURCE.get();
	}
	
	public static File getSourceDirectory() {
		return SOURCE_DIRECTORY.get();
	}
	
	public static URL getSourceResource(String path) {
		return SOURCE.get().getResource(path);
	}
	
	public static InputStream getSourceResourceAsStream(String path) {
		return SOURCE.get().getResourceAsStream(path);
	}
	
}
